from DCmotor import DCMotor
from machine import Pin, PWM
from time import sleep

#Variables for Wheels
frequency = 15000
D1_PHA_A = Pin(17, Pin.OUT)
D1_EN_A = PWM(Pin(16), frequency)
D1_STBY = Pin(4, Pin.OUT)
D1_PHA_B = Pin(0, Pin.OUT)
D1_EN_B = PWM(Pin(15), frequency)

D2_EN_A = PWM(Pin(12), frequency)
D2_PHA_A = Pin(13, Pin.OUT)
D2_STBY = Pin(14, Pin.OUT)
D2_EN_B = PWM(Pin(26), frequency)
D2_PHA_B = Pin(27, Pin.OUT)
FrR = DCMotor(D1_STBY, D1_PHA_A, D1_EN_A, 350, 1023)
RrR = DCMotor(D1_STBY, D1_PHA_B, D1_EN_B, 350, 1023)
FrL = DCMotor(D2_STBY, D2_PHA_A, D2_EN_A, 350, 1023)
RrL = DCMotor(D2_STBY, D2_PHA_B, D2_EN_B, 350, 1023)

def forwardsD(speed):
    print('forwards ' + str(speed))
    FrL.forward(speed)
    FrR.forward(speed)
    FrR.forward(speed)
    RrR.forward(speed)

def backwardsD(speed):
    print('backwards ' + str(speed))
    FrL.backwards(speed)
    FrL.backwards(speed)
    FrR.backwards(speed)
    RrR.backwards(speed)

def leftD(speed):
    print('left ' + str(speed))
    FrL.backwards(speed)
    FrL.backwards(speed)
    FrR.forward(speed)
    RrR.forward(speed)

def rightD(speed):
    print('right ' + str(speed))
    FrL.forward(speed)
    FrL.forward(speed)
    FrR.backwards(speed)
    RrR.backwards(speed)

def brakeD():
    print('brake')
    FrL.stop()
    FrL.stop()
    FrR.stop()
    RrR.stop()