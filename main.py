import socket
from machine import Pin
import network
import time
from directdrive import forwardsD, backwardsD, leftD, rightD, brakeD
import DCmotor
import servofunctions

import esp
esp.osdebug(None)

import gc
gc.collect()

ssid = 'Dollarklaus'
password = ''
attempt = 1

station = network.WLAN(network.STA_IF)
station.active(True)

while station.isconnected() == False:
    print('Connection attempt #' + str(attempt))
    station.connect(ssid, password)
    time.sleep(10)
    attempt += 1

print('Connection successful')
print(station.ifconfig())

###########
#main######

def web_page():
    html = """<html><head><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="icon" href="data:,"><style>html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}.button{display: inline-block; background-color: #e7bd3b; border: none;border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}</style><script></script></head><body><p><a href="/?autopilot=on"><button class="button">Autopilot ON</button></a></p><p><a href="/?autopilot=off"><button class="button">Autopilot OFF</button></a></p><p><p>Speed: <input type="text" id="speed" name="speed"><br></p><p><a href="/?forwardsD"><button class="button">Forwards</button></a></p><p><a href="/?backwardsD"><button class="button">Backwards</button></a></p><p><a href="/?brakeD"><button class="button">Brake</button></a></p><p><a href="/?leftD"><button class="button">Left</button></a></p><p><a href="/?rightD"><button class="button">Right</button></a></p></p><p><p><a href="/?pickup"><button class="button">Pick Up</button></a></p><p><a href="/?putdown"><button class="button">Put Down</button></a></p><p><a href="/?open"><button class="button">Brake</button></a></p><p><a href="/?close"><button class="button">Left</button></a></p></p></body></html>"""
    return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    request = conn.recv(1024)
    request = str(request)
    print('Content = %s' % request)
    if request.find('/?autopilot=on') == 6:
        print('Autopilot ON')
        autopilot_on()
    if request.find('/?autopilot=off') == 6:
        print('Autopilot OFF')
        autopilot_off()
    if request.find('/?forwardsD') == 6: #The D stands for direct, not sure if direct drive functions are required.
        print('forwardsD')
        forwardsD(50)
    if request.find('/?backwardsD') == 6:
        print('backwardsD')
        backwardsD(50)
    if request.find('/?leftD') == 6:
        print('leftD')
        leftD(50)
    if request.find('/?rightD') == 6:
        print('rightD')
        rightD(50)
    if request.find('/?brakeD') == 6:
        print('brakeD')
        brakeD()
    response = web_page()
    conn.send('HTTP/1.1 200 OK\n')
    conn.send('Content-Type: text/html\n')
    conn.send('Connection: close\n\n')
    conn.sendall(response)
    conn.close()

