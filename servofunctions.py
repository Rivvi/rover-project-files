from time import sleep
import machine
from servo import Servo
delay = 0.01
min = 15
max = 165

base_servo = Servo(machine.Pin(23))
#Base Servo min max mid (15,165,83)

x_servo = Servo(machine.Pin(22))
#X position servo min max mid(20,110,65)

y_servo = Servo(machine.Pin(19))
#y position servo min max (70,130)

claw_servo = Servo(machine.Pin(21))
#claw position open, closed (110,80)
def put_down():
    sleep(1)
    x_servo.write_angle(110)
    sleep(1)
    y_servo.write_angle(130)
    
def pick_up():
    sleep(1)
    x_servo.write_angle(20)
    sleep(1)
    y_servo.write_angle(130)

def open_claw():
    sleep(1)
    claw_servo.write_angle(160)

def close_claw():
    sleep(1)
    claw_servo.write_angle(110)
